# Stackoverflow app
The test project for piano.io company

## Install

- JDK 
- Gradle
- Tomcat

## Setting 

Tomcat:

- Use ".\src\main\webapp\WEB-INF\web.xml"
- Project compiler output: ".\src\main\webapp\WEB-INF\classes:

Gradle:

- Use gradle task configuration (from project)

Run:

- Check unit test: TestStack.java
- http://localhost:8080

Start server:

 - catalina run

### Stack

- Java
- Servlet
- Unit test
- Gradle

### Features

- Using servlet for http-requests
- Deserialization json response from stackexchange
- Error processing
- Using unit test.

