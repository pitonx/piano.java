<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Error!</title>
</head>
<body>
<h1>
    Error:&nbsp;${requestScope.error}
</h1>

<p>
    Back to <a href="\">main</a>
</p>

</body>
</html>
