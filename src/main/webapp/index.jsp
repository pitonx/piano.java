<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page errorPage="WEB-INF/jsp/error.jsp" %>

<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="./css/style.css"/>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <title>Stackexchange</title>
</head>
<body>

<div class="content">
    <div class="search-box">
        <form action="/search" class="search-box-form" method="post">

            <h2 class="search-box-title">
                Enter your question
            </h2>

            <input type="text" class="search-box-form__input" name="query"/>
            <input type="submit" class="search-box-form__btn" value="Search"/>
        </form>
    </div>

    <div class="result-box">

        <c:if test="${not empty requestScope.error}">
            <h3> Error </h3>

            <ul class="result-list">
                <li>error_id: ${requestScope.error.getErrorId()} </li>
                <li>error_message: ${requestScope.error.getErrorMessage()} </li>
                <li>error_name: ${requestScope.error.getErrorName()} </li>
            </ul>

        </c:if>

        <c:if test="${requestScope.questionsList.size() == 0}">
            <h3>Nothing found</h3>
        </c:if>

        <ul class="result-list">
            <c:forEach var="question" items="${requestScope.questionsList}">

                <li class="result-list__item">
                    <div class="result result-right">
                        <div class="author">

                            <c:if test="${question.getIsAnswered()}">
                                <span class="author-answers" style="background-color: #CCFFCC">Answered&nbsp;</span>
                            </c:if>

                            <span class="author-name">${question.getOwner().getDisplayName()}</span>
                            <span class="author-date">${question.getCreationDate()}</span>
                            <span class="author-answers">${question.getAnswerCount()} answers</span>
                        </div>
                        <div class="question">
                            <p class="question-text">
                                Q:&nbsp; <a href="${question.getLink()}"> ${question.getTitle()}</a>
                            </p>
                        </div>
                    </div>
                </li>

            </c:forEach>
        </ul>

    </div>
</div>

</body>
</html>
