package model;

public class Owner {
    private long reputation;
    private long user_id;
    private String user_type;
    private long accept_rate;
    private String profile_image;
    private String display_name;
    private String link;

    // ---

    public String getDisplayName() {
        return this.display_name;
    }
}
