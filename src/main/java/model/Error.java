package model;

public class Error {
    private int error_id;
    private String error_message;
    private String error_name;

    // ---

    public int getErrorId() {
        return this.error_id;
    }

    public String getErrorMessage() {
        return this.error_message;
    }

    public String getErrorName() {
        return this.error_name;
    }
}
