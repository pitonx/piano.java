package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Question {
    private List<String> tags;
    private List<Comment> comments;
    private Owner owner;
    private boolean is_answered;
    private long view_count;
    private long protected_date;
    private long accepted_answer_id;
    private long answer_count;
    private long score;
    private long last_activity_date;
    private long creation_date;
    private long last_edit_date;
    private long question_id;
    private String link;
    private String title;

    // ---

    public boolean getIsAnswered() {
        return this.is_answered;
    }

    // answer_count
    public Owner getOwner() {
        return this.owner;
    }

    // answer_count
    public long getAnswerCount() {
        return this.answer_count;
    }

    // creation_date
    public String getCreationDate() {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(new Date(this.creation_date * 1000));
    }

    // question_id
    public long getQuestionId() {
        return this.question_id;
    }

    // link
    public String getLink() {
        return this.link;
    }

    // title
    public String getTitle() {
        return this.title;
    }
}
