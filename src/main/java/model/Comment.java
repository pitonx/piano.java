package model;

public class Comment {
    private Owner owner;
    private ReplyToUser reply_to_user;
    private boolean edited;
    private long score;
    private long creation_date;
    private long post_id;
    private long comment_id;
}
