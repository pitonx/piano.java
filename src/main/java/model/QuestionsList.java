package model;

import java.util.LinkedList;
import java.util.List;

public class QuestionsList {
    private List<Question> items = new LinkedList<>();

    public List<Question> getItems() {
        return this.items;
    }
}
