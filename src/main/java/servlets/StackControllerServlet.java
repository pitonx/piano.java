package servlets;

import helper.JsonTools;
import model.QuestionsList;

import helper.WebTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/search")
public class StackControllerServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Получим параметры с формы
        String query = req.getParameter("query");

        // На query.isEmpty() реагировать не будем т.к. в этом случае выведим все вопросы
        if (query == null) {
            query = "";
        }

        // Запрос к stackexchange
        String json = "";

        try {
            json = WebTool.Search(query);
            if (json.trim().isEmpty())
                throw new Exception("");
        } catch (Exception e) {
            req.setAttribute("error", "Something went wrong");
            req.getRequestDispatcher("WEB-INF/jsp/error.jsp").forward(req, resp);
            return;
        }

        // Условие проверки ошибки, которую может вернуть stackexchange и десериализайция ответа
        if (json.contains("error_id")) {
            model.Error error = (new JsonTools<model.Error>()).getDeserializeClass(json, model.Error.class);
            req.setAttribute("error", error);
        } else {
            QuestionsList questionsList = (new JsonTools<QuestionsList>()).getDeserializeClass(json, QuestionsList.class);
            req.setAttribute("questionsList", questionsList.getItems());
        }

        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/");
    }
}