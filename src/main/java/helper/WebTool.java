package helper;

import java.io.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.net.URLEncoder;

/**
 * Tools for request
 */
public class WebTool {

    /**
     * URl for search
     */
    public static String Search(String query) throws Exception, UnsupportedEncodingException {
        return GetPage(String.format("/2.2/search/advanced?order=desc&sort=votes&site=stackoverflow&q=%1$s", URLEncoder.encode(query, java.nio.charset.StandardCharsets.UTF_8.toString())));
    }

    /**
     * URL detail page
     */
    public static String QuestionById(String questionId) throws Exception, UnsupportedEncodingException {
        return GetPage(String.format("/2.2/questions/%1$s?order=desc&sort=activity&site=stackoverflow&filter=!-*f(6rRC8e6K", questionId));
    }

    // ---

    // URL domain
    private static final String URL_SOURCE = "http://api.stackexchange.com";

    /**
     * Get page
     */
    private static String GetPage(String query) throws Exception {

        StringBuilder sb = new StringBuilder();

        try (CloseableHttpClient httpClient = HttpClients.createDefault();) {

            HttpGet httpGet = new HttpGet(URL_SOURCE.concat(query));
            httpGet.setHeader("Content-type", "application/json");

            InputStream inputStream = null;

            try {
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity entity = response.getEntity();

                inputStream = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }

            } catch (Exception e) {
                throw e;
            } finally {
                try {
                    if (inputStream != null) inputStream.close();
                } catch (Exception squish) {
                    // Давим
                }
            }
        } catch (IOException ioe) {
            throw ioe;
        }

        return sb.toString();
    }
}
