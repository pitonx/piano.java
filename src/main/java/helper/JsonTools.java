package helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;

public class JsonTools<T> {

    /**
     *  ...
     * @param json
     * @param typeClass
     * @return
     */
    public T getDeserializeClass(String json, Class<T> typeClass) {
        return (new GsonBuilder().create()).fromJson(json, typeClass);
    }

    /**
     *
     * @param json
     * @return
     */
    public static Map<String, String> getPostParams(String json) {
        Gson gson = new GsonBuilder().create();
        return (Map<String, String>) gson.fromJson(json, Object.class);
    }
}
