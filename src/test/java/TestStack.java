import helper.JsonTools;
import junit.framework.TestCase;
import model.QuestionsList;
import helper.WebTool;

public class TestStack extends TestCase {

    private String json = null;

    protected void setUp() throws Exception {
        json = WebTool.Search("Example");
    }

    protected void tearDown() throws Exception {
        json = null;
    }

    // Проверим, что работает web-запрос
    public void testRequest() throws Exception {
        assertNotNull(json);
        assertFalse(json.isEmpty());
    }

    // Протестируем десерелизацию
    public void testGetQuestionsList() throws Exception {
        QuestionsList questionsList = (new JsonTools<QuestionsList>()).getDeserializeClass(json, QuestionsList.class);
        assertTrue(questionsList.getItems().size() > 0);
    }
}
